package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Events_003Test extends BaseTest{
	private final String YEAR = "2019";
  @Test
  public void verifyYearBasedcalendar() throws Throwable{
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.onboardPatientpage.navigationToEvents();
	  initializePages.eventsPage.Eva_CCA_Events_003(YEAR);
  }
}
