package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_103Test extends BaseTest{
	private final String MOOD1="Sad";
	private final String MOOD2="Happy";
	
	@Test
	public void verifySubjectField() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.oconversationpage.Eva_CCA_Onboard_Patient_103(MOOD1,MOOD2);
	}
}