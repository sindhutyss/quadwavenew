package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_106Test extends BaseTest{
	private final String SUBJECT1 = "Travel";
	private final String SUBJECT2 = "Food";
 
  @Test
  public void validateSubjectField() throws Throwable{
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.oconversationpage.Eva_CCA_Onboard_Patient_106(SUBJECT1,SUBJECT2);
  }
}