package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Events_011Test extends BaseTest{
  @Test
  public void verifyDateField() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.navigationToEvents();
		initializePages.eventsPage.Eva_CCA_Events_011();
  }
}