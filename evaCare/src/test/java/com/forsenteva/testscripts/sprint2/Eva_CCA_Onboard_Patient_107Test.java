package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_107Test extends BaseTest{
	private final String PETS = "Pets";
	
	@Test
  public void verifyMultipleDataSubject() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.oconversationpage.Eva_CCA_Onboard_Patient_107(PETS);
  }
}
