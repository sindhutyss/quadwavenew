package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_033Test extends BaseTest{
  @Test
  public void validateEmergencyContactField() throws Throwable {
	  final String EMERGENCY_CONTACT_NAME = "aaAASWS";
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.onBoardAddNewPage.Eva_CCA_Onboard_Patient_033(EMERGENCY_CONTACT_NAME);
  }
}
