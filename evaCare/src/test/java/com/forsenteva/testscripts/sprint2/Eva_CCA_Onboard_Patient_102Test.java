package com.forsenteva.testscripts.sprint2;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_102Test extends BaseTest{
	private final String MOOD1="Sad";
	@Test
	public void verifyMoodFieldNotMultiValue() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.oconversationpage.Eva_CCA_Onboard_Patient_102(MOOD1);
	}
}
