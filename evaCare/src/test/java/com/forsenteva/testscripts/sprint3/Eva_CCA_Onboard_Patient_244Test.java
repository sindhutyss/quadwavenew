package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_244Test extends BaseTest{
	private String DEVICE_NAME = "Redmi 6A";
	@Test
	public void displayNewDeviceDetails() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.deviceRegistrationPage.Eva_CCA_Onboard_Patient_244(DEVICE_NAME);
	}
}