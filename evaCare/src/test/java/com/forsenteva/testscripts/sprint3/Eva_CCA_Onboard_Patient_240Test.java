package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

import net.bytebuddy.dynamic.NexusAccessor.InitializationAppender;

public class Eva_CCA_Onboard_Patient_240Test extends BaseTest{
  @Test
  public void verifyDeviceRegistrationCode() throws Throwable {
	  InitializePages initializationPages = new InitializePages(driver);
	  initializationPages.deviceRegistrationPage.Eva_CCA_Onboard_Patient_240();
  }
}
