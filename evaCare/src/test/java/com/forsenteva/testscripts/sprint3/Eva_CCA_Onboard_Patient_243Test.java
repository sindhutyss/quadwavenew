package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_243Test extends BaseTest{
	private final String DEVICE_NAME = "Device Name";
	private final String SERIAL_NUMBER = "Serial Number";
	private final String MOBILE_NUMBER = "Mobile Number";
	private final String STATUS = "Status";

	@Test
	public void verifyFieldsInDeviceREgistartionPage() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.deviceRegistrationPage.Eva_CCA_Onboard_Patient_243(DEVICE_NAME, SERIAL_NUMBER, MOBILE_NUMBER, STATUS);
	}
}