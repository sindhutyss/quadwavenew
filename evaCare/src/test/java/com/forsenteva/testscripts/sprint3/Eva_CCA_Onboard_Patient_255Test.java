package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_255Test extends BaseTest{
	// Test data 
	private final String MOBILE_NUMBER = "9620954266";
	
	@Test
  public void f() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.deviceRegistrationPage.Eva_CCA_Onboard_Patient_255(MOBILE_NUMBER);
  }
}
