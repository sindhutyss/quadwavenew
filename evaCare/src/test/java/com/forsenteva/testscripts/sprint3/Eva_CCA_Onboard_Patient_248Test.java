package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_248Test extends BaseTest{
	@Test
	public void verifyAutoPopulatedDetails() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.navigationToDeviceRegistrationPage();
		initializePages.deviceRegistrationPage.Eva_CCA_Onboard_Patient_248();
	}
}
