package com.forsenteva.testscripts.sprint3;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

import net.bytebuddy.dynamic.NexusAccessor.InitializationAppender;

public class Eva_CCA_Onboard_Caregiver_030 extends BaseTest{
  private final String RELATIONSHIP = "Caregiver";
	
	@Test
  public void f() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.onboardCaregiverPage.Eva_CCA_Onboard_Caregive_030(RELATIONSHIP);
  }
}
