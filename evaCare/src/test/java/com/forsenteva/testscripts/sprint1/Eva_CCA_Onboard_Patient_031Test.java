package com.forsenteva.testscripts.sprint1;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_031Test extends BaseTest{
  private final String PHONE_NUMBER = "123456@#$";
	
	
	@Test
  public void verifyField() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_031(PHONE_NUMBER);
  }
}
