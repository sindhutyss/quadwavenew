package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_085Test extends BaseTest{
	@Test
	public void Eva_CCA_Onboard_Patient_085AndEva_CCA_Onboard_Patient_086() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_085();
	}
}
