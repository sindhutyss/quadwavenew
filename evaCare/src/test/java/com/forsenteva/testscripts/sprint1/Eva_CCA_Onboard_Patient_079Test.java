package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class Eva_CCA_Onboard_Patient_079Test extends BaseTest{

	@Test
	public void Eva_CCA_Onboard_Patient_079() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_079();
	}
}