package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_084Test extends BaseTest{
	// Test data
	public final String PATIENT = "vvv";
	public final String PERSONAL_INTERESTS = "Personal Interests";
	public final String MUSIC_MOVIES = "Music and Movies";
	public final String PERSONALIZED_MEDIA = "Personalized Media Library";
	public final String NO_RECORDS_FOUND = "No Results Found";

	/*
	 * Test to verify error message in pesonalized media library page when no records present
	 */
	@Test
	public void verifyPrsonalizedMediaPageWithoutData() throws Throwable {
		// Initialization of page objects
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_084();
		}
}