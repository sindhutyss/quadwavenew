package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_097Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	private final String QUESTION = "@#$%^";
	private final String MOOD = "Agitated";
	private final String SUBJECT = "Baseball Sport";

	@Test
	public void verifyPhraseIsMandatory() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_097(QUESTION,SUBJECT,MOOD);
	}
}