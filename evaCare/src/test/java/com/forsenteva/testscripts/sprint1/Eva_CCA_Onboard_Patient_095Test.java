package com.forsenteva.testscripts.sprint1;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;


public class Eva_CCA_Onboard_Patient_095Test extends BaseTest{
  @Test
  public void verifyNavigationToConversation() throws Throwable {
	  InitializePages initializePages = new InitializePages(driver);
	  initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_095();
  }
}
