package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.library.WebActionUtil;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class Eva_CCA_Onboard_Patient_082Test extends BaseTest{
	// Test data 

	private final String TAGS= "AAA@w";
	private final String MUSIC = "BBB#";
	private final String MOVIE = "CDDKKDF";
	private final String ARTIST = "123454";

	@Test
	public void verifyPageNavigation() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_082(TAGS,MUSIC,MOVIE,ARTIST,6);
	}
}