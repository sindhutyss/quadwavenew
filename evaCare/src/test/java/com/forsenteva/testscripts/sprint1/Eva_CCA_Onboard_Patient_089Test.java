package com.forsenteva.testscripts.sprint1;

import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_089Test extends BaseTest{
	/*
	 * Defining all test data
	 */
	public final String SEARCH_dATA = "music";

	@Test
	public void verifyConversationpageNavigation() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_089();
	}
}