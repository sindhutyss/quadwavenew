package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;

public class Eva_CCA_Onboard_Patient_090Test extends BaseTest{
	//test data
	public final String SEARCH_DATA = "CCC";
	
  @Test
  public void verifyConversationPage() throws Throwable {
		InitializePages initializePages = new InitializePages(driver);
	    
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_090(SEARCH_DATA);	
  }
}
