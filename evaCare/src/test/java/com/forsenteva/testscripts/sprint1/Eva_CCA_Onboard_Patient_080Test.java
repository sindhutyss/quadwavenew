package com.forsenteva.testscripts.sprint1;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.init.InitializePages;
import com.forsenteva.web.library.BaseTest;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.OnBoardPatientPage;

public class Eva_CCA_Onboard_Patient_080Test extends BaseTest{
	// Test data 
	private final String TAG = "aa@#$%";

	@Test
	public void verifyTextFieldForSpecialCharacters() throws Throwable {	
		InitializePages initializePages = new InitializePages(driver);
		initializePages.onboardPatientpage.Eva_CCA_Onboard_Patient_080(TAG);
		Thread.sleep(3000);	
	}
}