package com.forsenteva.web.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.forsenteva.web.library.BasePage;
import com.forsenteva.web.library.WebActionUtil;

public class ConversationPage extends BasePage{
	public ConversationPage(WebDriver driver) {
		super(driver);
	}


	@FindBy(xpath = "//div[@id='form-group-CommunicationForm.AddNew']/descendant::button[@id='CommunicationForm.AddNew']")
	private WebElement conversationAddNewButton;

	@FindBy(xpath = "//div[@id='form-group-PatientName']/descendant::input[@id='PatientName']")
	private WebElement patientName;

	@FindBy(xpath = "//div[@id='form-group-Mood']/descendant::label[@for='Mood']")
	private WebElement moodLabel;

	@FindBy(xpath = "//div[@id='Mood']/descendant::span[text()='Please select']")
	private WebElement moodDropdown;

	@FindBy(xpath = "//div[@id='Subjects']//input")
	private WebElement subjectDropdown;

	@FindBy(xpath = "//div[@id='form-group-Subjects']/descendant::p[text()='Please Select Subject']")
	private WebElement errorMessage;

	@FindBy(xpath = "//div[@id='form-group-Submit']/descendant::button[@id='Submit']")
	private WebElement submitButton;

	@FindBy(xpath = "//div[@id='form-group-AnswerType']/descendant::input[@id='AnswerType-Answer']")
	private WebElement answerRadiobutton;

	@FindBy(xpath = "//div[@id='form-group-AnswerType']/descendant::input[@id='AnswerType-Media']")
	private WebElement mediaRadiobutton;

	@FindBy(xpath = "//div[@id='form-group-Answer']/descendant::textarea[@name='Answer']")
	private WebElement answerTextArea;


	/*
	 * verify user can enter data into mood field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_102(String mood) throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "Mood dropdown");

		try {
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + mood + "']")).click();

			String value= WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + mood + "']")).getText();
			Assert.assertEquals(value, mood);
			WebActionUtil.logger.info("Selected mood value is successfully displayed");
		}
		catch( AssertionError a) {
			WebActionUtil.logger.info("Failed to select mood value");
		}
	}

	/*
	 * validate selected data is displaying or not
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_103(String moodValue1,String moodValue2) throws Throwable{
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		WebActionUtil.clickOnElementUsingJS(moodDropdown, "Mood dropdown");
		try {
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + moodValue1 + "']")).click();

			String actualValue = WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + moodValue1 + "']")).getText();
			Assert.assertEquals(actualValue, moodValue1);

			WebActionUtil.clickOnElementUsingJS(moodDropdown, "Mood dropdown");
			WebActionUtil.driver.findElement(By.xpath("//span[@class='ng-binding ng-scope'][text()='" + moodValue1 + "']")).click();

			String actualValue2 = WebActionUtil.driver.findElement(By.xpath("//div[@id='Mood']//span[text()='" + moodValue2 + "']")).getText();
			Assert.assertEquals(actualValue2.contains(actualValue), false);
			WebActionUtil.logger.info("mood value is single value select");
		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("mood value is not single value select");

		}
	}

	/*
	 * validate multiple selection in the mood field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_104() throws Throwable{
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectDropdown, "Subject dropdown");
		List<WebElement> subjects = WebActionUtil.driver.findElements(By.xpath("//span[@class='ui-select-choices-row-inner']"));

		try {
			Assert.assertEquals(subjects.size()>1, true);
			String clas = moodLabel.getAttribute("class");
			Assert.assertEquals(clas.contains("required"), true);
			WebActionUtil.logger.info("subject is a dropdown and it is a mandatory field");
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("subject is a dropdown and it is a mandatory field");
		}
	}

	/*
	 * verify entering data into subject field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_106(String value1, String value2) throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectDropdown, "Subject dropdown");
		WebActionUtil.clearAndTypeText(subjectDropdown, value1, "Subject dropdown");
		try {
			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + value1 + "']")).click();
			WebElement subjectElement = WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + value1 + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);

			WebActionUtil.typeText(subjectDropdown, value2, "subjectDropdown");
			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']//span[text()='" + value2 + "']")).click();


			WebElement subject2=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']//span[text()='" + value2 + "']"));
			Assert.assertEquals(subject2.isDisplayed(), true);


			WebActionUtil.logger.info("subject dropdown is validated");

		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("Could not able to validate subject dropdown");
		}
	}

	/*
	 * validate multiple selection in subject field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_107(String petsData) throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");

		WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
		WebActionUtil.clearAndTypeText(subjectDropdown, petsData, "subjectDropdown");

		try {
			WebActionUtil.logger.info("checking whether selected element is displayed");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + petsData + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + petsData + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);
			WebActionUtil.logger.info("Could not able to validate subject dropdown");

		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("Could not able to validate subject dropdown");

		}
	}

	/*
	 * validate selected data is displaying or not
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_108(String pets,String travel, String comedy)  throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");

		WebActionUtil.logger.info("checking whether selected elements is displayed");
		try {
			// first entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, pets, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + pets + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + pets + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);


			// second entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, travel, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + travel + "']")).click();
			WebElement subjectElement1=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + travel + "']"));
			Assert.assertEquals(subjectElement1.isDisplayed(), true);

			// third entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, comedy, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + comedy + "']")).click();
			WebElement subjectElement2=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + comedy + "']"));
			Assert.assertEquals(subjectElement2.isDisplayed(), true);
			WebActionUtil.logger.info("selected elements is displayed succesfully");

			List<WebElement> removeElements = WebActionUtil.driver.findElements(By.xpath("//div[@id='Subjects']/descendant::span[contains(@class,' ui-select-match-close')]"));
			for(WebElement element : removeElements) {
				Assert.assertEquals(WebActionUtil.isElementDisplayedOrNot(element), true);
			}

			WebActionUtil.logger.info("remove elements for added data is available");
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("remove elements for added data is not available");
		}
	}

	/*
	 * validate remove data from subject field
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_109(String subject1,String subject2, String subject3)  throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");

		WebActionUtil.logger.info("checking whether selected elements is displayed");
		try {
			// first entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, subject1, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + subject1 + "']")).click();
			WebElement subjectElement=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + subject1 + "']"));
			Assert.assertEquals(subjectElement.isDisplayed(), true);


			// second entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, subject2, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + subject2 + "']")).click();
			WebElement subjectElement1=WebActionUtil.driver.findElement(By.xpath(" //div[@id='Subjects']/descendant::span[text()='" + subject2 + "']"));
			Assert.assertEquals(subjectElement1.isDisplayed(), true);

			// third entry
			WebActionUtil.clickOnElementUsingJS(subjectDropdown, "subjectDropdown");
			WebActionUtil.clearAndTypeText(subjectDropdown, subject3, "subjectDropdown");

			WebActionUtil.driver.findElement(By.xpath(" //span[@class='ui-select-choices-row-inner']/descendant::span[text()='" + subject3 + "']")).click();
			WebElement subjectElement2=WebActionUtil.driver.findElement(By.xpath("//div[@id='Subjects']/descendant::span[text()='" + subject3 + "']"));
			Assert.assertEquals(subjectElement2.isDisplayed(), true);
			WebActionUtil.logger.info("selected elements is displayed succesfully");
			List<WebElement> removeElements = WebActionUtil.driver.findElements(By.xpath("//div[@id='Subjects']/descendant::span[contains(@class,' ui-select-match-close')]"));

			for(WebElement element : removeElements) {
				WebActionUtil.clickOnElementUsingJS(element, "remove element");
			}

			Assert.assertEquals(WebActionUtil.isElementDisplayedOrNot(errorMessage), true);
			WebActionUtil.isElementNotEnabled(submitButton);
			WebActionUtil.logger.info("Succesful in displaying message when user remove all selected data");
		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("failed in displaying message when user remove all selected data");
		}
	}

	/*
	 * verify user can remove all data from subject field or not
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_110() throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		WebActionUtil.logger.info("checking both elements of response field are radio button");
		try {
			String typeAtt = answerRadiobutton.getAttribute("type");
			Assert.assertEquals(typeAtt, "radio");

			String mediaAtt = mediaRadiobutton.getAttribute("type");
			Assert.assertEquals(mediaAtt, "radio");
			Assert.assertEquals(answerRadiobutton.isSelected(), true);
			WebActionUtil.logger.info("successful in verifying response field");

		}
		catch(AssertionError a) {
			WebActionUtil.logger.info("Failed in verifying response field");
		}
	}

	/*
	 * verify the response type field
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_111() throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		String attribtValue = answerRadiobutton.getAttribute("type");
		System.out.println(attribtValue);
		try {
			Assert.assertEquals(attribtValue, "radio");
			Assert.assertEquals(answerRadiobutton.isSelected(), true);
			WebActionUtil.logger.info("Succesfully response type radio button does not allow user to enter data into it");
		}
		catch(AssertionError a ) {
			WebActionUtil.logger.info("Response type field failed to behave as a radio button because it accepted data sent to it");
		}
	}

	/*
	 * verify after selecting answer option from Response type field
	 *
	 * 
	 * 
	 */
	public void Eva_CCA_Onboard_Patient_113(String data) throws Throwable {
		OnBoardPatientPage.navigationToconversationPage();
		WebActionUtil.sleep(3);
		WebActionUtil.clickOnElement(conversationAddNewButton, "click on add new button");
		WebActionUtil.sleep(3);

		String tagName = answerTextArea.getTagName();
		try {
			Assert.assertEquals(tagName, "textarea");
			WebActionUtil.clearAndTypeText(answerTextArea, data, "enter data into answer text field");
			data.replace("\n", Keys.chord(Keys.SHIFT, Keys.ENTER));

			WebActionUtil.logger.info("succesful in entering multiple lines of data into answer text area");
		}
		catch(AssertionError e) {
			WebActionUtil.logger.info("Failed to enter multiple lines of code in answer text area");
		}
	}
}