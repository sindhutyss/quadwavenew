package com.forsenteva.web.init;

import org.openqa.selenium.WebDriver;

import com.forsenteva.web.pages.ConversationPage;
import com.forsenteva.web.pages.DeviceRegistrationPage;
import com.forsenteva.web.pages.EventsPage;
import com.forsenteva.web.pages.HomePage;
import com.forsenteva.web.pages.Login_Page;
import com.forsenteva.web.pages.OnBoardPatientPage;
import com.forsenteva.web.pages.OnBoardPatientProfilePage;
import com.forsenteva.web.pages.OnboardAddNewPage;
import com.forsenteva.web.pages.OnboardCaregiverPage;

public class InitializePages
{
	public Login_Page oLogin_Page;
	public HomePage homepage;
	public OnBoardPatientPage onboardPatientpage;
	public ConversationPage oconversationpage;
	public OnboardAddNewPage onBoardAddNewPage;
	public EventsPage eventsPage;
	public OnboardCaregiverPage onboardCaregiverPage;
	public OnBoardPatientProfilePage onboardProfilePage;
	public DeviceRegistrationPage deviceRegistrationPage;
	
	public InitializePages(WebDriver driver) 
	{	
		oLogin_Page = new Login_Page(driver);
		homepage = new HomePage(driver);
		onboardPatientpage= new OnBoardPatientPage(driver);
		oconversationpage = new ConversationPage(driver);
		onBoardAddNewPage = new OnboardAddNewPage(driver);
		eventsPage = new EventsPage(driver);
		onboardCaregiverPage = new OnboardCaregiverPage(driver);
		onboardProfilePage = new OnBoardPatientProfilePage(driver);
		deviceRegistrationPage = new DeviceRegistrationPage(driver);
	}
}