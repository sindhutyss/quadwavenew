package com.forsenteva.web.library;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.forsenteva.web.library.Generic;
import com.forsenteva.web.library.LogStatus;
import com.forsenteva.web.listener.MyExtentListener;



public class WebActionUtil
{
	public static WebDriver driver;
	static WebDriverWait wait;
	public long ETO = 5;
	public Generic generic;
	public static JavascriptExecutor jsExecutor;
	public Actions action;
	public final static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public WebActionUtil(WebDriver driver, long ETO, Generic generic)
	{
		this.driver = driver;
		this.ETO = ETO;
		wait = new WebDriverWait(driver, ETO);
		this.generic = generic;
		jsExecutor = (JavascriptExecutor) driver;
		action = new Actions(driver);
	}
	/* Wait till the page to load */
	@SuppressWarnings("unused")
	public static void waitTillPageLoad(WebDriver driver, int seconds)
	{
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		jsExecutor = (JavascriptExecutor) driver;
		// Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = wd -> ((JavascriptExecutor) driver)
				.executeScript("return document.readyState").toString().equals("complete");
		// Get JS is Ready
		boolean jsReady = (Boolean) jsExecutor.executeScript("return document.readyState").toString().equals("complete");
		// Wait Javascript until it is Ready!
		if (!jsReady)
		{
			System.out.println("JS in NOT Ready!");
			// Wait for Javascript to load
			wait.until(jsLoad);
		}
		else
		{
			sleep(2);
		}
	}
	public static String capture(WebDriver driver)
	{
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String sDate = sdf.format(date);
		String destPath = "C:\\Users\\Megha\\Desktop\\Sindhu\\evaCare\\ScreenShot"  + sDate + ".png";
		try
		{
			File f = new File(destPath);
			if (!(f.exists()))
			{
				f.createNewFile();
			}
			FileUtils.copyFile(src, f);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return destPath;
	}
	/* Verify the Element is Clickable or Not */
	public static boolean isElementClickable(WebElement element, String elementName) throws Throwable {
		// waitTillPageLoad();
		sleep(5);
		try {
			logger.info("---------Method is Element clickable  ---------");
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		}
		catch (Exception e)
		{
			MyExtentListener.logger
			.fail(MarkupHelper.createLabel(
					"Verify user is able to mouse hover on " + "\'" + elementName + "\'"
							+ " ||  User is not able to mouse hover on " + "\'" + elementName + "\'",

							ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Element is not visible or not clickable " + elementName);
		}
		return false;
	}
	/* Click on the Element */
	public static void clickOnElement(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("---------Verifying element is displayed or not ---------");
				waitTillPageLoad(driver, 30);
				element.click();
				MyExtentListener.logger.pass("Verify user is able to click on " + "\'" + elementName + "\'"
						+ " ||  User is able to click on " + "\'" + elementName + "\'");
			}
			else
			{
				logger.info("-------Element is not Clickable--------------");
			}
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	public void submitOnElement(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("---------Verifying element is displayed or not ---------");
				waitTillPageLoad(driver, 30);
				element.submit();
				MyExtentListener.logger.pass("Verify user is able to click on " + "\'" + elementName + "\'"
						+ " ||  User is able to click on " + "\'" + elementName + "\'");
			}
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + "  || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Assert.fail("unable to Click on " + "\'" + elementName + "\'");
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + elementName
					+ "\'" + " || User is not able to click on " + "\'" + elementName + "\'", ExtentColor.RED));
			try
			{
				MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			}
			catch (IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			throw e;
		}
	}
	/* Click on the Element */
	public void enterOnElement(WebElement element, String elementName , String value) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("----------- Verfiying user is able to click on the Element----------");
				waitTillPageLoad(driver, 30);
				try
				{
					waitForElement(element, driver, elementName, 20);
				}
				catch (Throwable e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				element.sendKeys(Keys.ENTER);
			}
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel(
					"Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
							+ " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
							ExtentColor.RED));

			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to type on " + elementName);
			// } catch (Exception e) {
			// //MyExtentListener.logger.fail(MarkupHelper.createLabel(
			// "Verify user is able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'"
			// + " || User is not able to type " + "\'" + value + "\'" + "in " + "\'" + elementName + "\'",
			// ExtentColor.RED));
			//
			// MyExtentListener.logger.addScreenCaptureFromPath(capture(driver, element));
			// Assert.fail("Unable to type in " + elementName);
		}
	}

	/* Press tab */
	public static void pressTab(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("----------- Verfiying user is able to press tab key from keyboard----------");
				waitTillPageLoad(driver, 30);
				try
				{
					waitForElement(element, driver, elementName, 20);
				}
				catch (Throwable e)
				{
					e.printStackTrace();
				}
				element.sendKeys(Keys.TAB);
			}
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel(
					"Verify user is able to press tab from keyboard || User is not able to press tab key from keyboard " + elementName + "\'",
					ExtentColor.RED));

			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to press tab key  " + elementName);

		}
	}

	/* Click on the Element using JavaSCript */
	public static  void clickOnElementUsingJS(WebElement element, String elementName) throws Throwable
	{
		try
		{
			if (isElementClickable(element, elementName))
			{
				logger.info("-----------Clicking on the Element Using JS----------");
				jsExecutor.executeScript("arguments[0].click();", element);
			}
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper
					.createLabel("Unable to Click on " + "\'" + elementName + "\'", ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver)); // exception
			Assert.fail("Unable to Click on " + "\'" + elementName + "\'");
		}
	}
	/* Verify the Element is Displayed or Not */
	public static void isElementDisplayed(WebElement element, String elementName , int minutes) throws Throwable
	{
		try
		{
			logger.info("---------Waiting for visibility of element---------" + element);
			waitTillPageLoad(driver, 30);
			long timeout = minutes;
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			logger.info("---------Element is visible---------" + element);
		}
		catch (Exception e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
		catch (AssertionError e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}
	/* Wait for The element to Load or to Display */
	public static void waitForElement(WebElement element, WebDriver driver, String eleName, int minutes)
			throws Throwable
	{
		try
		{
			logger.info("---------Waiting for visibility of element---------" + element);
			waitTillPageLoad(driver, 30);
			long timeout = minutes;
			Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(timeout))
					.pollingEvery(Duration.ofMillis(250)).ignoring(NoSuchElementException.class);
			Assert.assertTrue(wait.until(ExpectedConditions.visibilityOf(element)) != null);
			logger.info("---------Element is visible---------" + element);
		}
		catch (Exception e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
		catch (AssertionError e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			logger.info("---------Element is not visible---------" + element);
			throw e;
		}
	}
	/* To Enter the Text to the Text filed */
	public static void typeText(WebElement element, String value, String elementName) throws Throwable
	{

		try
		{
			waitTillPageLoad(driver, 20);
			waitForElement(element, driver, elementName, 1);
			logger.info("Enter the value into" + elementName);
			element.sendKeys(value);
			logger.info("User is able to type " + value + " into " + elementName);
		}
		catch (AssertionError error)
		{
			logger.info(" User is not able to type " + value + " into " + elementName);
			// MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type on " + elementName);
		}
		catch (Exception e)
		{
			logger.info(" User is not able to type " + value + "into " + elementName);
			// MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type in " + elementName);
		}
	}
	/* Clear the Text field and Enter the Text to the Text filed */
	public static void clearAndTypeText(WebElement element, String value, String elementName)
	{
		try
		{
			logger.info("Clear the Field and Enter the Text ");
			sleep(3);
			element.clear();
			logger.info(elementName + " is cleared");
			element.sendKeys(value);
			logger.info(value + " is entered in " + elementName);
			logger.info(" User is able to type " + value + " into " + elementName);
		}
		catch (AssertionError error)
		{
			logger.info( "User is not able to type " + value + " into " + elementName);
			Assert.fail("Unable to type on " + "\'" + elementName + "\'");
		}
		catch (Exception e)
		{
			logger.info("User is not able to type " + value + " into " + elementName);
			// MyExtentListners.test.addScreenCaptureFromPath(capture(driver, element));
			Assert.fail("Unable to type in " + "\'" + elementName + "\'");
		}
	}


	/* Get the Text From the Page */
	public static String getText(WebElement element, String elementName) throws Throwable
	{
		logger.info("Get the text from the element:");
		String eleText = null;
		try
		{
			waitTillPageLoad(driver, 20);
			isElementDisplayed(element, elementName, 1);
			eleText = element.getText();
			if (eleText.equals(null))
			{
				logger.info("Unable to fetch text from " + "\'" + elementName);
				// MyExtentListners.test.addScreenCaptureFromPath(capture(driver)); // exception
				Assert.fail("Unable to fetch text from " + "\'" + elementName + "\'");
			}
		}
		catch (Exception e)
		{
			logger.info( "Unable to fetch text from " + "\'" + elementName + "\'");
			// MyExtentListners.test.addScreenCaptureFromPath(capture(driver)); // exception
			Assert.fail("Unable to fetch text from " + elementName);
		}
		return eleText;
	}
	/* Scroll to the End of the page */
	public void scrollToEndOfThePage() throws Throwable
	{
		sleep(5);
		logger.info("----------Scrolling till End of the Page-------");
		try
		{
			jsExecutor.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			logger.info("---------Successfully Scrolled till End of the Page---------");
		}
		catch (Exception e)
		{
			MyExtentListener.logger
			.fail(MarkupHelper.createLabel(" Unable to scroll to End of the Page " , ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		}
	}
	/* Scroll to the Element */
	public  static void scrollToElement(WebElement element, String elementName) throws Throwable
	{
		waitTillPageLoad(driver, 20);
		logger.info("-------------Scrolling till the Element------------");
		try
		{
			jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);
		}
		catch (Exception e)
		{
			MyExtentListener.logger
			.fail(MarkupHelper.createLabel(" Unable to scroll to an element " + element, ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		}
	}
	/* Scroll to the particular pixel */
	public static void scrollByPixel(double xpixels, double ypixels) throws Throwable
	{
		waitTillPageLoad(driver, 20);
		try
		{
			logger.info("Scrolling X-axis=" + xpixels + "and Y-axis=" + ypixels);
			jsExecutor.executeScript("window.scrollBy(" + xpixels + "," + ypixels + ")");
			logger.info("----------Scrolling Completed---------");
		}
		catch (Exception e)
		{
			MyExtentListener.logger
			.fail(MarkupHelper.createLabel(" Unable to scroll", ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
		}
	}
	/* Verify the Text */
	public static void verifyElementText(WebElement element, String expectedText)
	{
		sleep(3);
		String actualText = element.getText();
		sleep(3);
		Assert.assertEquals(actualText, expectedText);
		logger.info(actualText + " is matching with " + expectedText);
	}
	/* Verify the page Title */
	public void verifyTheTitle(String expectedTitle)
	{
		sleep(3);
		String actualTitle = driver.getTitle();
		logger.info(":" + actualTitle);
		Assert.assertEquals(actualTitle, expectedTitle);
		logger.info("Compare 'Actual title' with the 'Expected Title' ");
		logger.info( actualTitle + " is matching with " + expectedTitle);
	}
	/* Wait in seconds */
	public static void sleep(long seconds)
	{
		try
		{
			Thread.sleep(seconds * 1000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	/* Get the Web Element */
	/* Send the name Address of the element and name of the locator */
	public WebElement getWebElement(String locatorType, String locatorValue)
	{
		waitTillPageLoad(driver, 20);
		try
		{
			By by = (By) By.class.getDeclaredMethod(locatorType, String.class).invoke(null, locatorValue);
			WebElement element = driver.findElement(by);
			return element;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	/* Navigate To URL */
	public void navigateToUrl(String url)
	{
		waitTillPageLoad(driver, 0);
		logger.info("Navigate to Url");
		driver.navigate().to(url);
	}
	/* Refresh the Page */
	public void refreshThePage()
	{
		driver.navigate().refresh();
		waitTillPageLoad(driver, 20);
	}
	/* navigate To back */
	public void navigateToback()
	{
		waitTillPageLoad(driver, 20);
		logger.info("Navigate back ");
		driver.navigate().back();
	}
	/* Verifying the Element Displayed Or Not */
	public static boolean isElementDisplayedOrNot(WebElement element)
	{
		sleep(3);
		boolean actual = element.isDisplayed();
		Assert.assertEquals(actual, true, "Element is Not Displayed");
		return actual;
	}

	/* Switch To Tab */
	public void switchToTab(int tabindex)
	{
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(tabindex));
	}
	/* Switch To Window */
	public void switchToWindow(String nameOrHandle)
	{
		try
		{
			logger.info("---------Verifying Window is displayed or not ---------");
			ArrayList<String> listOfWindow = new ArrayList<String>(driver.getWindowHandles());
			driver.switchTo().window(listOfWindow.get(0));
			MyExtentListener.logger.pass("Verify user is able to click on " + "\' "
					+ " ||  User is able to click on " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to click on " + "\'" + " || User is not able to click on " + "\'", ExtentColor.RED));
			throw e;
		}
	}
	/* Switching into the frame */
	public void switchToFrame(WebElement element)
	{
		try
		{

			waitTillPageLoad(driver, 25);
			logger.info("---------Verifying Frame is displayed or not ---------");
			logger.info("Switching into the frame");
			driver.switchTo().frame(element);
			MyExtentListener.logger.pass("Verify user is able to  Switch to Frame " + "\' "
					+ " ||  User is able to Switch to Frame " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Switch to Frame " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Switch to Frame" + "\'" + " || User is not able to Switch to Frame " + "\'", ExtentColor.RED));
			throw e;
		}
	}
	/* Switching into the top window/first frame */
	public void defaultWindowContent()
	{
		waitTillPageLoad(driver, 30);
		logger.info("Switching into the top window/first frame");
		driver.switchTo().defaultContent();
		MyExtentListener.logger.pass("Verify user is able to  Switch to Default Content " + "\' "
				+ " ||  User is able to Switch to Default Content " + "\'");
	}
	/* Verify the check box is clicked or not and to click if not clicked */
	public static void clickCheckBox(WebElement element, String elementname)
	{

		try
		{waitTillPageLoad(driver, 30);
		if (element.isSelected())
		{
			logger.info("The check box is clicked");
		} else
		{
			logger.info("Click on check box ");
			element.click();
			logger.info( "The check box is clicked");
		}
		MyExtentListener.logger.pass("Verify user is able to  ClickCheck Box " + "\' "
				+ " ||  User is able to Click Checkbox " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Click Checkbox " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Click Checkbox" + "\'" + " || User is not able to Click Checkbox" + "\'", ExtentColor.RED));
			throw e;
		}


	}
	/* Upload file */
	public void upload(String imagePath, String string)
	{
		StringSelection stringSelection = new StringSelection(imagePath);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
		Robot robot = null;
		try
		{
			robot = new Robot();
		}
		catch (AWTException e)
		{
			e.printStackTrace();
		}
		robot.keyPress(KeyEvent.VK_CONTROL);
		sleep(1);
		robot.keyPress(KeyEvent.VK_V);
		sleep(1);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		sleep(1);
		robot.keyPress(KeyEvent.VK_ENTER);
		sleep(1);
		robot.keyRelease(KeyEvent.VK_ENTER);
		MyExtentListener.logger.pass("Verify user is able to  Upload " + "\' "
				+ " ||  User is able to Upload " + "\'");
	}
	/* Actions Class methods */

	/* Hover the mouse over element */
	public void mouseHover(WebElement element, String elementName)
	{
		try
		{
			sleep(10);
			// waitTillPageLoad();
			logger.info("Hovering over the " + elementName);
			Actions action = new Actions(driver);
			action.moveToElement(element).build().perform();
			MyExtentListener.logger.pass("Verify user is able to MouseHover " + "\' "
					+ " ||  User is able to MouseHover " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to MouseHover " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to MouseHover" + "\'" + " || User is not able to MouseHover" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	public void mouseHover2(WebElement element, String elementName)
	{
		try
		{
			sleep(10);
			logger.info("Hovering over the " + elementName);
			Actions action = new Actions(driver);
			action.sendKeys(element, Keys.ENTER).perform();
			MyExtentListener.logger.pass("Verify user is able to MouseHover " + "\' "
					+ " ||  User is able to MouseHover " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to MouseHover " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to MouseHover" + "\'" + " || User is not able to MouseHover" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	/* Drag And Drop */
	public void dragAndDrop(WebElement source, WebElement target)
	{
		try
		{
			logger.info("Drag the " + source + " and Drop to" + target);
			sleep(1);
			action.dragAndDrop(source, target).perform();
			MyExtentListener.logger.pass("Verify user is able to Drag and Drop " + "\' "
					+ " ||  User is able to Drag and Drop " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Drag and Drop " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Drag and Drop" + "\'" + " || User is not able to Drag and Drop" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	/* Double Click On Element */
	public void doubleClickOnElement(WebElement element)
	{
		try
		{
			logger.info("Double click on the " + element);
			action.doubleClick(element).perform();
			MyExtentListener.logger.pass("Verify user is able to Double Click on " + element
					+ " ||  User is able to Double Click on " + element );
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Double Click on " + element +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Double Click on " + element + " || User is not able to Double Click on " + element, ExtentColor.RED));
			throw e;
		}
	}

	/* Arrow Down */
	public void arrowDown()
	{
		try
		{
			logger.info("Press Arrow Key Down");
			action.sendKeys(Keys.ARROW_DOWN).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key down " + "\' "
					+ " ||  User is able to Press Arrow Key down " + "\'");
		}

		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Down " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Down" + "\'" + " || User is not able to Press Arrow Key Down" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	/* Arrow Up */
	public void arrowUp()
	{
		try
		{
			logger.info( "Press Arrow Key Up");
			action.sendKeys(Keys.ARROW_UP).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key Up " + "\' "
					+ " ||  User is able to Press Arrow Key Up " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Up " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Up" + "\'" + " || User is not able to Press Arrow Key Up" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	/* Arrow Up And Enter */
	public void arrowUpAndEnter()
	{
		try
		{
			logger.info("Press Arrow Key Down And Press Enter");
			action.sendKeys(Keys.ARROW_UP, Keys.ENTER).perform();
			MyExtentListener.logger.pass("Verify user is able to Press Arrow key Up & Press Enter" + "\' "
					+ " ||  User is able to Press Arrow Key Up & Press Enter " + "\'");
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Up & Press Enter " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Up & Press Enter" + "\'" + " || User is not able to Press Arrow Key Up & Press Enter" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	/* Arrow Down And Enter */
	public void arrowDownAndEnter()
	{
		try
		{
			logger.info("Press Arrow Key Down And Press Enter");
			action.sendKeys(Keys.ARROW_DOWN, Keys.ENTER).perform();
		}
		catch (AssertionError error)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Down & Press Enter " + "\'" +
					"\'", ExtentColor.RED));
			throw error;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Down & Press Enter" + "\'" + " || User is not able to Press Arrow Key Down & Press Enter" + "\'", ExtentColor.RED));
			throw e;
		}
	}

	public void pressShiftEndDelete()
	{
		logger.info("Press Shift End Delete");
		try {
			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_SHIFT);
			r.keyPress(KeyEvent.VK_END);
			sleep(1);
			r.keyRelease(KeyEvent.VK_END);
			r.keyRelease(KeyEvent.VK_SHIFT);
			sleep(1);
			r.keyPress(KeyEvent.VK_DELETE);
			sleep(1);
			r.keyRelease(KeyEvent.VK_DELETE);
			logger.info("Delete Completed");

		} catch (AWTException e) {
			logger.info("Not able to delete the Content");
			e.printStackTrace();
		}
	}
	/* Scroll to the End of the page */
	public void scrollToTopOfThePage()
	{
		try
		{
			logger.info("Scrolling till the Top of the page");
			waitTillPageLoad(driver, 30);
			logger.info("Scrolling till the Top of the page");
			jsExecutor.executeScript("window.scrollTo(0, -document.body.scrollHeight)");
			MyExtentListener.logger.pass("Verify user is able to select element ||  User is able to select element");
		}

		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to Press Arrow Key Down & Press Enter" + "\'" + " || User is not able to Press Arrow Key Down & Press Enter" + "\'", ExtentColor.RED));
			Assert.fail("Unable to scroll to top of the page");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("User is not able to scroll the page",ExtentColor.RED));
			throw e;
		}
	}

	/*
	 * @author:Aatish
	 *
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */

	public static boolean isSelected(WebElement element, String elementName) throws Throwable {
		sleep(5);
		try {
			logger.info("Verify whether element is selected");
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeSelected(element));
			MyExtentListener.logger.pass("User is able to select element" + element);
			return true;
		}
		catch (Exception e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to verify user is able to select element " + elementName);
			MyExtentListener.logger.fail(MarkupHelper.createLabel("User is not able to select element" + "\'" + elementName + "\'",ExtentColor.RED));
		}
		return false;
	}

	public void logMessage(LogStatus status, String msg)
	{
		System.out.println("Status:" + status + " Msg:" + msg);
	}

	/*
	 * @author:Aatish
	 *
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	public static boolean isAlertPresent(WebDriver driver, int sec) {	
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(sec, TimeUnit.SECONDS)
				.pollingEvery(250, TimeUnit.MILLISECONDS).ignoring(UnreachableBrowserException.class);
		boolean alerPresent = wait.until(ExpectedConditions.alertIsPresent()) != null;
		if (alerPresent) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * @author:Aatish
	 *
	 * Description: This method checks for visibility of alert and waits till
	 * sec provided by and returns true if visible else false
	 */
	public static void clear(WebElement element,  String elementName) throws IOException
	{
		try
		{
			logger.info("Clear the Field and Enter the Text ");
			sleep(3);
			element.clear();
			MyExtentListener.logger.pass("User is able to clear data from element" + element);
		}
		catch (Exception e)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to type in " + "\'" + elementName + "\'");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("user is not able to clear data from element " + element +
					"\'", ExtentColor.RED));
		}
	}

	/*
	 * @author: Sindhu N 
	 *
	 * Description: This method returns all options present in dropdown
	 * 
	 */
	public static List<WebElement> getDropDownOptions(WebElement element) throws IOException {

		try {
			logger.info("selecting value from dropdown");
			Select dropDown = new Select(element);
			List<WebElement> options = dropDown.getOptions();
			logger.info("Succesful in capturing the dropdown options");
			MyExtentListener.logger.pass("User is able to capture options from dropdown" + element);
			return options;
		}

		catch(AssertionError error) {
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to capture options from dropdown ");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select value from dropdown " + element +
					"\'", ExtentColor.RED));
		}
		return null;	
	}

	/*
	 * @author: Sindhu N 
	 *
	 * Description: This method selects value in dropdown based on visible text
	 * 
	 */

	/* selecting dropdown by value */
	public static void selectDropdownByValue(WebElement element, String value) throws IOException {
		try {
			logger.info("selecting value from dropdown");
			Select dropDown = new Select(element);
			dropDown.selectByVisibleText(value);
			MyExtentListener.logger.pass("User is able to select value from dropdown" + element);
		}
		catch(AssertionError error ) {
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to capture options from dropdown ");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("user is not able to select value from dropdown " + element +
					"\'", ExtentColor.RED));
			throw error;
		}
	}

	/*
	 * @author: Sindhu N 
	 *
	 * Description: This method checks whether the element is enabled or not 
	 * 
	 */
	public static void isElementEnabled(WebElement element) throws IOException{
		try {
			logger.info("Verifying whether element is enabled" + element);
			WebElement e=element;
			boolean actual = e.isEnabled();
			Assert.assertEquals(actual, true, "Element is Not Enabled");
			MyExtentListener.logger.pass("Verify user is able to verify element is enabled" + element
					+ " ||  User is able to verify element is enabled" + element );
		}
		catch(AssertionError error) {
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to verify element is enabled");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify element is enabled" + element +
					"\'", ExtentColor.RED));
			throw error;
		}	
	}

	public static String formatDuration(final long millis) {
		long seconds = (millis / 1000) % 60;
		long minutes = (millis / (1000 * 60)) % 60;
		long hours = millis / (1000 * 60 * 60);

		StringBuilder b = new StringBuilder();
		b.append(hours == 0 ? "00" : hours < 10 ? String.valueOf("0" + hours) :
			String.valueOf(hours));
		b.append(":");
		b.append(minutes == 0 ? "00" : minutes < 10 ? String.valueOf("0" + minutes) :    
			String.valueOf(minutes));
		b.append(":");
		b.append(seconds == 0 ? "00" : seconds < 10 ? String.valueOf("0" + seconds) :
			String.valueOf(seconds));
		return b.toString();
	}

	/*
	 * @author: Shivraj
	 *
	 * 
	 * 
	 */
	public static void verifyAttributeValue(WebElement element, String attributeName, String attributeValue) throws IOException {
		try {
			logger.info("Getting the attribute value of " + element);
			String attribute = element.getAttribute(attributeName);
			boolean value = attribute.contains(attributeValue);
			Assert.assertEquals(value, true, "Attribute value not matching");
			MyExtentListener.logger.pass("Verify user is able to verify attribute value of an element" + element
					+ " ||  User is able to verify attribute value of an element" + element );
		} catch (AssertionError error)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to verify attribute value of an element");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify attribute value of an element" + element +
					"\'", ExtentColor.RED));
			throw error;
		}
	}

	/*
	 * @author: Shivraj
	 *
	 * 
	 * 
	 */
	public static void isElementNotEnabled(WebElement element) throws IOException {
		try {
			logger.info("checking whether element is enabled");
			boolean actual = element.isEnabled();
			Assert.assertNotEquals(actual, true, "Element is Enabled");
			MyExtentListener.logger.pass("Verify user is able to verify whether element not enabled" + element
					+ " ||  User is able to verify whether element not enabled" + element );
		}catch (AssertionError error)
		{
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("Unable to verify whether element is not enabled");
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to verify whether element is not enabled" + element +
					"\'", ExtentColor.RED));
			throw error;
		}
	}

	/*
	 * @author: Atish
	 *
	 * 
	 * 
	 */
	public static String generateEmail( String domain, int length)
	{
		try {    
			logger.info("Creating a Random String for Email");
			MyExtentListener.logger.pass("Creating a Random Email of" + domain +"|| Able to create Random Email of "+domain);
			return RandomStringUtils.random(length, "abcdefghijklmnopqrstuvwxyz") + "@" + domain;
		}    
		catch (Exception e)
		{
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Creating a Random Email of " + domain + " || Unable to create Random Email of " + domain, ExtentColor.RED));
			throw e;
		}    
	}



	//File upload by Robot Class
	public void uploadFileWithRobot (String imagePath) {
		StringSelection stringSelection = new StringSelection(imagePath);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, null);
		Robot robot = null;
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		robot.delay(250);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(150);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public static void verifySpecialCharacter(String name) throws IOException {
		try {
			logger.info("---------Verifying  Special character present are not---------");
			String specialCharacters = " !#$%&'()*+,-./:;<=>?@[]^_`{|}~0123456789[a-zA-Z]";
			String str2[] = name.split(".");
			int count = 0;
			for (int i = 0; i < str2.length; i++) {
				if (specialCharacters.contains(str2[i])) {
					count++;
				}
			}
			if (name != null && count == 0) {
				System.out.println("Special char is present");
			} else {
				System.out.println("Special char is not present");
			}
			MyExtentListener.logger.pass("Verify Special character are present" + "\'" + name + "\'"
					+ " ||  User is able to select " + "\'" + name + "\'");
		} catch (Exception e) {
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify Special character are not present " + "\'" + name + "\'"
					+ " ||  User is not able to select " + "\'" + name + "\'", ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("User is not able to select " + "\'" + name + "\'");
		}
	}


	public static void selectbyVisibletext(WebDriver driver, WebElement element, String text) throws IOException {
		try {
			waitTillPageLoad(driver, 30);
			Select sel = new Select(element);
			sel.selectByVisibleText(text);
			MyExtentListener.logger.pass("Verify user is able to select " + "\'" + text + "\'"
					+ " ||  User is able to select " + "\'" + text + "\'");
		} catch (Exception e) {
			MyExtentListener.logger.fail(MarkupHelper.createLabel("Verify user is able to select " + "\'" + text + "\'"
					+ " ||  User is not able to select " + "\'" + text + "\'", ExtentColor.RED));
			MyExtentListener.logger.addScreenCaptureFromPath(capture(driver));
			Assert.fail("User is not able to select " + "\'" + text + "\'");
		}
	}
}