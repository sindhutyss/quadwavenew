package com.forsenteva.web.library;

import java.io.File;

public class Generic 
{
public String filePath = "";
	
	/* To print the log/test result messages into text file/json file */
	public void logMessage(LogStatus status, String msg) {
		System.out.println("Status:" + status + " Msg:" + msg);
		
	}
	/* To Create a Folder */
	public void createFolder(String FolderPath) {
		File dir = new File(FolderPath);
		if (!dir.exists())
			dir.mkdirs();
	}

	/* To create a File */
	public void createFile(String FolderPath, String fileName) throws Exception {
		File f = new File(FolderPath + "/" + fileName);
		if (f.exists()) {
			System.out.println("File already present");
		} else {
			f.createNewFile();
			System.out.println("Created new file");
		}
	}
}
